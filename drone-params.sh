#!/bin/sh
set -euo pipefail

function asis {
    echo $1
}
function camel {
    echo $(snake $1) | awk -F _ '{printf "%s", $1; for(i=2; i<=NF; i++) printf "%s", toupper(substr($i,1,1)) substr($i,2); print"";}'
}
function dot {
    echo $(snake $1) | tr '_' '.'
}
function kebab {
    echo $(snake $1) | tr '_' '-'
}
function snake {
    echo $1 | tr '[:upper:]' '[:lower:]'
}

_prefix=${1:-""}
_case=${2:-"asis"}
_assign=${3-"="}
_quote=${4-"'"}
_filter=${5:-""}

_params=""
IFS=$'\n'
for _kv in $(env); do
    _key=$(echo "$_kv" | cut -d '=' -f 1)
    _val="$(echo "$_kv" | cut -d '=' -f 2)"
    case $_key in PLUGIN_${_filter}*)
        _key=${_key#PLUGIN_$_filter}
        _params="$_params ${_prefix}$($_case $_key)${_val:+$_assign}${_val:+$_quote}${_val}${_val:+$_quote}"
        env | grep -q "^$_key=" || export $_key="$_val"
    esac
done

printf %s\\n "$_params"
